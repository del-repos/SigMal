# Sig•Mal

##  _"Security in Development, Development for Security"_

## Traversing the Repo
| Directory | Description
|:---:   |:----
|[educational-materials](./educational-material) |  Collection of slides and information by semesters 
|[projects](./projects) | Projects from Sig•Mal 

## About

Currently NJIT's premier Security Oriented Special Interest Group for Security under ACM.

<p align="center">
    <img src="https://raw.githubusercontent.com/NJIT-ACM/NJIT-ACM/main/ACMLOGO.png" alt="" data-canonical-src="" width="300" height="300">
</p>

## Goal

* Build up interest and skills within Security for NJIT ACM with insight into Malware Analysis and Practical Advice for CTFs

> POG ~ Linus Torvalds (T-Daddy)


