# Educational Material

Below are rough outlines of topics that were covered during each semester. The semesters alternate between defensive and offensive security topics.

- [Semester 1](sem1/)
    - Types of malware analysis
    - Hashing of files
    - Fuzzy hashing of files
    - File analysis
    - Reverse engineering via decompilation
    - Static malware analysis using Ghidra
- [Semester 2](sem2/)
    - Reverse engineering
    - Port scanning
    - Firewalls
    - Web security
    - Buffer overflows
- [Semester 3](sem3/)
    - CTF practicals
    - Basic malware analysis
    - Projects
- [Semester 4](sem4/)
    - Graphs: Control Flow Graphs, Malware Visualization
    - Crypto: Hashing, Encryption
    - Hatching Triage
    - Malware Development
    - Ghidra (Reverse Engineering)
